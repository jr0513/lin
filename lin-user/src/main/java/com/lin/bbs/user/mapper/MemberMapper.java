package com.lin.bbs.user.mapper;

import com.lin.bbs.api.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

}
