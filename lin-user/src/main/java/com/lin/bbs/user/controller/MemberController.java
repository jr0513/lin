package com.lin.bbs.user.controller;


import com.lin.bbs.api.entity.Admin;
import com.lin.bbs.api.service.AdminService;
import com.lin.bbs.common.api.CommonResult;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/member")
public class MemberController {

    @Reference(check = false)
    private AdminService adminService;

    @GetMapping("/test")
    public CommonResult test(){
        List<Admin> list = adminService.list();
        return CommonResult.success(list);
    }

}
