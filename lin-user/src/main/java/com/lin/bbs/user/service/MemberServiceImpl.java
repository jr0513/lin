package com.lin.bbs.user.service;

import com.lin.bbs.api.entity.Member;
import com.lin.bbs.user.mapper.MemberMapper;
import com.lin.bbs.api.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;


/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@Service
@Component
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

}
