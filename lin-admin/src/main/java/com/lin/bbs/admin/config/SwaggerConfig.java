package com.lin.bbs.admin.config;

import com.lin.bbs.common.config.BaseSwaggerConfig;
import com.lin.bbs.common.domain.SwaggerProperties;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger API文档相关配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends BaseSwaggerConfig {

    @Override
    public SwaggerProperties swaggerProperties() {
        return SwaggerProperties.builder()
                .apiBasePackage("com.lin.bbs.admin.controller")
                .title("Lin后台系统")
                .description("Lin后台相关接口文档")
                .contactName("Jzhou")
                .version("1.0")
                .enableSecurity(true)
                .build();
    }
}
