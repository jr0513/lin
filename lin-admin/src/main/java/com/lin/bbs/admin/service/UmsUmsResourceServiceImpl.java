package com.lin.bbs.admin.service;

import com.lin.bbs.api.entity.UmsResource;
import com.lin.bbs.admin.mapper.UmsResourceMapper;
import com.lin.bbs.api.service.UmsResourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 后台资源表 服务实现类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@Service
public class UmsUmsResourceServiceImpl extends ServiceImpl<UmsResourceMapper, UmsResource> implements UmsResourceService {

    @Resource
    private UmsResourceMapper resourceMapper;

    @Override
    public List<UmsResource> findlist() {
        List<UmsResource> umsResources = resourceMapper.selectList(null);
        return umsResources;
    }
}
