package com.lin.bbs.admin.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lin.bbs.admin.mapper.AdminMapper;
import com.lin.bbs.api.dto.AdminParam;
import com.lin.bbs.api.entity.Admin;
import com.lin.bbs.api.service.AdminCacheService;
import com.lin.bbs.api.service.AdminService;
import com.lin.bbs.common.exception.Asserts;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 后台用户表 服务实现类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-01
 */
@Service
@Component
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Resource
    private AdminMapper adminMapper;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private AdminCacheService adminCacheService;

    @Override
    public Admin getAdminByUsername(String username) {
        // 1、先查询Redis是否存在该用户
        Admin admin = adminCacheService.getAdmin(username);
        // 2、存在则直接返回
        if (!Objects.isNull(admin)) return admin;
        // 3、不存在则查询数据库
        Admin one = adminMapper.selectOne(new QueryWrapper<Admin>().eq(Admin.USERNAME, username));
        if (!Objects.isNull(one)){
            // 4、查询出的数据放入Redis中
            adminCacheService.setAdmin(one);
            // 5、返回数据
            return admin;
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return null;
    }

    @Override
    public Admin register(AdminParam adminParam) {
        // 1、查询数据库是否存在该用户，如果存在则返回null，不存在则添加到数据库
        Admin one = adminMapper.selectOne(new QueryWrapper<Admin>().eq(Admin.USERNAME, adminParam.getUsername()));
        if (!Objects.isNull(one)){
            return null;
        }
        // 2、转换Admin类型并赋值
        Admin admin = new Admin();
        BeanUtils.copyProperties(adminParam,admin);
        admin.setStatus(1);
        admin.setPassword(passwordEncoder.encode(admin.getPassword()));
        int insert = adminMapper.insert(admin);
        if (insert!=1){
            Asserts.fail("添加账号失败，请联系系统管理员");
        }
        return admin;
    }

}
