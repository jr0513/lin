package com.lin.bbs.admin.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lin.bbs.api.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 后台用户表 Mapper 接口
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-01
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

}
