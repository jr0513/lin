package com.lin.bbs.admin.controller;


import com.lin.bbs.api.dto.AdminParam;
import com.lin.bbs.api.entity.Admin;
import com.lin.bbs.api.service.AdminService;
import com.lin.bbs.api.service.MemberService;
import com.lin.bbs.common.api.CommonResult;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * <p>
 * 后台用户表 前端控制器
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-01
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Reference(check = false)
    private MemberService memberService;

    @PostMapping("/register")
    @ApiOperation("注册后台管理员账户")
    public CommonResult<Admin> register(@Validated @RequestBody AdminParam adminParam){
        Admin admin = adminService.register(adminParam);
        if (Objects.isNull(admin)){
            return CommonResult.failed();
        }
        return CommonResult.success(admin);
    }

}
