package com.lin.bbs.admin.mapper;

import com.lin.bbs.api.entity.UmsResource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 后台资源表 Mapper 接口
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@Mapper
public interface UmsResourceMapper extends BaseMapper<UmsResource> {

}
