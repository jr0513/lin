package com.lin.bbs.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 后台资源表 前端控制器
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@RestController
@RequestMapping("/resource")
public class UmsResourceController {

}
