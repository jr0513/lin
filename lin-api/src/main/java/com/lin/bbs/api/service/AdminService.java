package com.lin.bbs.api.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.bbs.api.dto.AdminParam;
import com.lin.bbs.api.entity.Admin;
import org.springframework.security.core.userdetails.UserDetails;


/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-01
 */
public interface AdminService extends IService<Admin> {

    /**
     * 通过用户名获取后台用户
     * @param username 用户名
     * @return Admin
     */
    Admin getAdminByUsername(String username);

    /**
     * 获取用户信息
     * @param username 用户名
     * @return UserDetails
     */
    UserDetails loadUserByUsername(String username);

    /**
     * 创建后台管理员账户
     * @param adminParam 请求参数
     * @return Admin
     */
    Admin register(AdminParam adminParam);
}
