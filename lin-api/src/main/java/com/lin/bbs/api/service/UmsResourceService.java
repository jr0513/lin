package com.lin.bbs.api.service;

import com.lin.bbs.api.entity.UmsResource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 后台资源表 服务类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
public interface UmsResourceService extends IService<UmsResource> {

    /**
     * 查询出全部的的资源列表
     * @return 资源列表
     */
    List<UmsResource> findlist();
}
