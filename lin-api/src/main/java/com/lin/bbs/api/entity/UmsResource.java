package com.lin.bbs.api.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 后台资源表
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("ums_resource")
@ApiModel(value = "Resource对象", description = "后台资源表")
public class UmsResource implements Serializable {

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("资源名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("资源URL")
    @TableField("url")
    private String url;

    @ApiModelProperty("描述")
    @TableField("description")
    private String description;

    @ApiModelProperty("资源分类ID")
    @TableField("category_id")
    private Long categoryId;


    public static final String ID = "id";

    public static final String CREATE_TIME = "create_time";

    public static final String NAME = "name";

    public static final String URL = "url";

    public static final String DESCRIPTION = "description";

    public static final String CATEGORY_ID = "category_id";

}
