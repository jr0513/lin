package com.lin.bbs.api.service;

import com.lin.bbs.api.entity.Admin;

public interface AdminCacheService {

    /**
     * 获取缓存后台用户信息
     */
    Admin getAdmin(String username);

    void setAdmin(Admin one);
}
