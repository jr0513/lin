package com.lin.bbs.api.service;

import com.lin.bbs.api.entity.Member;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author Jzhou
 * @since 2022-04-02
 */
public interface MemberService extends IService<Member> {

}
